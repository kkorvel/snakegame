package game;

import java.awt.Color;


/**
 * @author Koit 
 * Antud objektide v�rvid.
 */
public enum TileType {
	Snake(Color.blue),
	Fruit(Color.MAGENTA),
	TileBlock(Color.BLACK),
	SuperScoreFruit(Color.YELLOW),
	SuperLengthFruit(Color.PINK),
	Empty(null);

	
	private Color tileColor;
	private TileType(Color color) {
		tileColor = color;
	}
	public Color getTileColor() {
		return tileColor;
	}
	public void setTileColor(Color tileColor) {
		this.tileColor = tileColor;
	}
	public Color getColor() {
		return tileColor;
	}
}	