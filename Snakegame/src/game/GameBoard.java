package game;

import java.awt.Graphics2D;

/**
 * @author Koit
 * Map suurus.
 */
public class GameBoard {
 
	public static final int TILE_SIZE = 26;
	public static final int MAP_SIZE = 26;

	
	
	/**
	 * Array trackib tile.
	 * Uus gameboard.
	 */
	private TileType[] tiles;
	public GameBoard() { 
		tiles = new TileType[MAP_SIZE * MAP_SIZE];
		resetBoard();
	}
	
	/**
	 * Resetib tiled.
	 */
	public void resetBoard() {
		for(int i = 0; i < tiles.length; i++ ) {
			tiles[i] = TileType.Empty; //resetib tiled
		}
	}

	/**
	 * @param x koordinaat.
	 * @param y koordinaat.
	 * @param type tile t��p.
	 * @return tagastab tiled.
	 */
	public void setTile(int x, int y, TileType type) {
		tiles[y * MAP_SIZE + x] = type;
	}
	
	public TileType getTile(int x, int y) {
		return tiles[y * MAP_SIZE + x];
	}	

	/**
	 * @param g Joonistab GameBoardi,
	 * XY koordinaadi arvutus,
	 * Kui tile fruit, siis muudab ennem, kui renderdab.
	 */
	public void draw(Graphics2D g) {
		for (int i = 0; i < tiles.length; i++) {
			int x = i % MAP_SIZE; 
			int y = i / MAP_SIZE; 
			
			if (tiles[i].equals(TileType.Fruit)) {
				g.setColor(TileType.Fruit.getColor());
				g.fillOval(x * TILE_SIZE + 4, y * TILE_SIZE + 4, TILE_SIZE - 8, TILE_SIZE - 8);
			} else if (tiles[i].equals(TileType.Snake)) {
				g.setColor(TileType.Snake.getColor());
				g.fillRect(x * TILE_SIZE + 1, y * TILE_SIZE + 1, TILE_SIZE - 2, TILE_SIZE - 2);
			} else if (tiles[i].equals(TileType.TileBlock)) {
				g.setColor(TileType.TileBlock.getColor());
				g.fillRect(x * TILE_SIZE + 1, y * TILE_SIZE + 1, TILE_SIZE - 2, TILE_SIZE - 2);
			} else if (tiles[i].equals(TileType.SuperScoreFruit)) {
				g.setColor(TileType.SuperScoreFruit.getColor());
				g.fillRect(x * TILE_SIZE + 1, y * TILE_SIZE + 1, TILE_SIZE + 5, TILE_SIZE + 5);
			} else if (tiles[i].equals(TileType.SuperLengthFruit)) {
				g.setColor(TileType.SuperLengthFruit.getColor());
				g.fillRect(x * TILE_SIZE + 1, y * TILE_SIZE + 1, TILE_SIZE - 2, TILE_SIZE - 2);
						
			}
		}
	}
}