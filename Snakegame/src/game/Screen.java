package game;
import java.awt.Canvas;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.WindowConstants;

public class Screen {
	private JFrame frame;
	private Canvas canvas;
	
	/**
	 * M�ngu taust.
	 */
	public Screen() {
		frame = new JFrame("Ussim�ng");
		canvas = new Canvas();
		frame.setLayout(new FlowLayout());
	}
	
	public Canvas getCanvas() {
		return canvas;
	}
	
	/**
	 * Display.
	 */
	public void display() {
		addPauseButton();
		setupCanvas();
		displayFrame();
	}
		
	/**
	 * Tagatausta v�rv.
	 * Tausta lisamine Jframe.
	 */
	private void setupCanvas() {
		canvas.setBackground(Color.white); //v�rv
		canvas.setPreferredSize(new Dimension(GameBoard.MAP_SIZE * GameBoard.TILE_SIZE,
				GameBoard.MAP_SIZE * GameBoard.TILE_SIZE));
		frame.add(canvas);
	}
	
	/**
	 * Frame packi lisamine, et window oleks �iges kohas.
	 */
	private void displayFrame() {
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}
	
	/**
	 * Pausi nupp.
	 */
	public void addPauseButton() { 
		JButton PauseButton = new JButton();
		PauseButton.setText("Paus");
		frame.add(PauseButton);
	}

}

	
		