package game;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * @author Koit
 * Mängu põhiline loogika.
 */
public class Engine extends KeyAdapter {
	private static final Font FONT_LARGE = new Font("Times New Roman", Font.BOLD, 35);
	private static final long UPDATES_PER_SECOND = 5;
	private Canvas canvas;
	private GameBoard gameboard;
	private Snake snake;
	private int skoor;
	private boolean gameOver;

	public Engine(Canvas canvas) {
		this.canvas = canvas;
		gameboard = new GameBoard();
		snake = new Snake(gameboard);
		resetGame();
		canvas.addKeyListener(this);
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
	 * Tegevused, mis juhtuvad klahvivajutusel.
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		switch(e.getKeyCode()) {
			case KeyEvent.VK_W:
				snake.setDirection(Direction.Up);
				break;
			case KeyEvent.VK_S:
				snake.setDirection(Direction.Down);
				break;
			case KeyEvent.VK_A:
				snake.setDirection(Direction.Left);
				break;
			case KeyEvent.VK_D:
				snake.setDirection(Direction.Right);
				break;
			case KeyEvent.VK_ENTER:
				if (gameOver) {
				 if(e.getKeyCode() == KeyEvent.VK_ENTER && gameOver) {
					resetGame();
				}
				 }	
				break;
			default:
				break;
				
		}
		
	}
	
	/**
	 * See funktsioon vastutab mängu ülesseadmise eest, samuti sisaldab 
	 * mängu loopi ja loogikat.
	 */
	public void startGame() {
		canvas.createBufferStrategy(3);
		Graphics2D g = (Graphics2D)canvas.getBufferStrategy().getDrawGraphics();
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		long start = 0L;
		long sleepDuration = 0L;
		long frameCounter = 0;
		while(true) {
			start = System.currentTimeMillis();
			update();
			frameCounter++;
			if (frameCounter >= 20*UPDATES_PER_SECOND){
				frameCounter = 0;
				spawnSuperScoreFruit();
				spawnSuperLengthFruit();
			}
			render(g);
			canvas.getBufferStrategy().show();
			g.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
			sleepDuration = (1000L / UPDATES_PER_SECOND) - (System.currentTimeMillis() - start);
			//kui on suurem kui null, siis läheb sleepi
			if(sleepDuration > 0) {
				try {
					Thread.sleep(sleepDuration);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	
	/**
	 * @param g Mäng muutub graafiliseks, skoori tekst.
	 */
	private void render(Graphics2D g) {
		gameboard.draw(g);

		g.setColor(Color.RED);

		if (gameOver) {
			g.setFont(FONT_LARGE);
			String message = new String("Lõppskoor: " + skoor);
			g.drawString(message, canvas.getWidth() / 2
					- g.getFontMetrics().stringWidth(message) / 2, 250);
			g.setFont(FONT_LARGE);
			message = new String("Vajuta Enter, et alustada uut mängu!");
			g.drawString(message, canvas.getWidth() / 2
					- g.getFontMetrics().stringWidth(message) / 2, 350);
		 } else {
			g.setFont(FONT_LARGE);
			g.drawString("Skoor: " + skoor, 15, 30);
			
		 }

	}

	/**
	 * Mängu loogika update ning snake skoor.
	 */
	private void update() {
		// loogika update
		if(gameOver || !canvas.hasFocus()) {
			return;
		}
		TileType snakeTile = snake.updateSnake();
		if (snakeTile == null || snakeTile.equals(TileType.Snake) || snakeTile.equals(TileType.TileBlock)) {
			gameOver = true;
		} else if (snakeTile.equals(TileType.Fruit)) {
			skoor += 10;
			spawnFruit();
		} else if (snakeTile.equals(TileType.SuperScoreFruit)) {
			skoor += 100;
		} else if (snakeTile.equals(TileType.SuperLengthFruit)) {
			skoor+= 50;
		}
	}	
	
	
	/**
	 * Tavalise fruiti paigutus.
	 */
	public void spawnFruit() {

		int random = (int)(Math.random() * ((GameBoard.MAP_SIZE * GameBoard.MAP_SIZE) - snake.getSnakeLength() - GameBoard.MAP_SIZE - 1));
		  
		int emptyFound = 0;
		int index = 0;
		while(emptyFound < random) {
			index++;
			if (gameboard.getTile(index % GameBoard.MAP_SIZE, index / GameBoard.MAP_SIZE).equals(TileType.Empty)) {
				emptyFound++;
			}
		}
		gameboard.setTile(index % GameBoard.MAP_SIZE, index / GameBoard.MAP_SIZE, TileType.Fruit);
	}
	
	/**
	 * SuperScoreFruiti paigutus.
	 */
	public void spawnSuperScoreFruit() {
		int random = (int)(Math.random() * ((GameBoard.MAP_SIZE * GameBoard.MAP_SIZE) - snake.getSnakeLength() - GameBoard.MAP_SIZE - 1));

		int emptyFound = 0;
		int index = 0;
		while(emptyFound < random) {
			index++;
			if (gameboard.getTile(index % GameBoard.MAP_SIZE, index / GameBoard.MAP_SIZE).equals(TileType.Empty)) {
				emptyFound++;
			}
		}
		gameboard.setTile(index % GameBoard.MAP_SIZE, index / GameBoard.MAP_SIZE, TileType.SuperScoreFruit);
	}
	
	/**
	 * SuperLengthFruiti paigutus.
	 */
	public void spawnSuperLengthFruit() {
		int random = (int)(Math.random() * ((GameBoard.MAP_SIZE * GameBoard.MAP_SIZE) - snake.getSnakeLength() - GameBoard.MAP_SIZE - 1));

		int emptyFound = 0;
		int index = 0;
		while(emptyFound < random) {
			index++;
			if (gameboard.getTile(index % GameBoard.MAP_SIZE, index / GameBoard.MAP_SIZE).equals(TileType.Empty)) {
				emptyFound++;
			}
		}
		gameboard.setTile(index % GameBoard.MAP_SIZE, index / GameBoard.MAP_SIZE, TileType.SuperLengthFruit);
	}
	/**
	 * TileBlocki paigutus.
	 */
	public void TileBlock() {
		int i = 1;
		for(i=1;i < GameBoard.MAP_SIZE;i++) {
			gameboard.setTile(i, i, TileType.TileBlock);
			gameboard.setTile(25, 25, TileType.Empty);}}


	/**
	 * See funktsioon resetib mängu ning paneb objektid esialgsesse kohta tagasi.
	 */
	private void resetGame() {
		 gameboard.resetBoard();
		 TileBlock();
		 snake.resetSnake();
		 skoor = 0;
		 spawnFruit();
		 spawnSuperScoreFruit();
		 spawnSuperLengthFruit();
		 gameOver = false;
	}
}