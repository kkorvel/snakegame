package game;

import java.awt.Point;
import java.util.LinkedList;

public class Snake {
	private final int MAX_BONUS_TILES = 4;
	
	private GameBoard board;
	
	private LinkedList<Point> points; //kogub snake punkte

	public Direction currentDirection;

	public Direction temporaryDirection;
	
	private int bonusTile = 0;
	
	/**
	 * @param board
	 * Vajalik snake pointsi jaoks
	 */
	public Snake (GameBoard board) {
	
		this.board = board;
		points = new LinkedList<Point>();
//see on snake pointsi jaoks
	}
	
	//snake reset
	/**
	 * Snake reset.
	 */
	public void resetSnake() {
		currentDirection = Direction.None;
		temporaryDirection = Direction.None;
		Point head = new Point(GameBoard.MAP_SIZE / 2, GameBoard.MAP_SIZE / 2);
		points.clear();
		points.add(head);
		board.setTile(head.x, head.y, TileType.Snake);
	}
	
	/**
	 * @return
	 * See funktsioon saab snake suuna, klaviatuuriklahvi
	 * vajutusega. Samuti saab snake pea asukoha.
	 * Saab snake asukoha, tagastab false, kui liigub vastu seina,
	 * teisel juhul lisab tile juurde.
	 * Updateb snake, snake pea asukohaga.
	 * vaatab tile t��pi enne update, kui polnud fruit, 
	 * siis eemaldab viimase punkti snakelt.
	 * Samuti uue tile lisamine.
	 */

	public TileType updateSnake() {

		currentDirection = temporaryDirection;
		Point head = points.getFirst();
		switch(currentDirection) { 
		 
			case Up:
				if(head.y <= 0) {
					return null;
				}
				points.push(new Point(head.x, head.y - 1));
				break;
			case Down:
				if(head.y >= GameBoard.MAP_SIZE - 1) {
					return null;
				}
				points.push(new Point(head.x, head.y + 1));
				break;
			case Left:
				if(head.x <= 0) {
					return null;
				}
				points.push(new Point(head.x - 1, head.y));
				break;
			case Right:
				if(head.x >= GameBoard.MAP_SIZE - 1) {
					return null;
				}
				points.push(new Point(head.x + 1, head.y));
				break;
			case None:
				return TileType.Empty;
	 	}
		
		head = points.getFirst();

		TileType oldType = board.getTile(head.x, head.y);
		if(!oldType.equals(TileType.Fruit) && !oldType.equals(TileType.SuperScoreFruit) && !oldType.equals(TileType.SuperLengthFruit)) {
			if (bonusTile > 0) {
				bonusTile--;
			} else {
				Point last = points.removeLast();
				board.setTile(last.x, last.y, TileType.Empty);
				oldType = board.getTile(head.x, head.y);				
			}
		} else if (oldType.equals(TileType.SuperLengthFruit)) {
			bonusTile += MAX_BONUS_TILES;
		}
			
			
			
		
		board.setTile(head.x, head.y, TileType.Snake);
		return oldType;
	}
	
	
	/**
	 * @param direction
	 * Selle funktsiooni abil pole v�imalik vastassuunas liikuda.
	 */
	public void setDirection(Direction direction) {//selle abil pole v�imalik vastassuunas liikuda
		if (direction.equals(Direction.Up) && currentDirection.equals(Direction.Down)) {
			return;
		} else if (direction.equals(Direction.Down) && currentDirection.equals(Direction.Up)) {
			return;
		} else if (direction.equals(Direction.Left) && currentDirection.equals(Direction.Right)) {
			return;
		} else if (direction.equals(Direction.Right) && currentDirection.equals(Direction.Left)) {
			return;
		}
		temporaryDirection = direction;
	 }

	/**
	 * @return
	 * Tagastab ussi suuruse.
	 */
	public int getSnakeLength() {
		return points.size();
	}
}		