import game.Engine;
import game.Screen;

/**
 * @author Koit
 * kood on tehtud http://psnbtech.blogspot.com.ee/2012/01/tutorial-java-creating-snake-game-part.html
 * tutoriali abiga!
 */
public class Main {
	public static void main (String[] args) {
		Screen screen = new Screen();
		Engine engine = new Engine(screen.getCanvas());
		
		screen.display();
		engine.startGame();

	}
}
